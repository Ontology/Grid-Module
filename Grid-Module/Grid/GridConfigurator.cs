﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using OntologyClasses.BaseClasses;

namespace Grid_Module.Grid
{
    public static class GridConfigurator
    {
        public static void ConfigureVisibility(DataGridColumn column, Type itemType)
        {
            if (typeof (clsOntologyItem) == itemType)
            {
                if (column.Header.ToString() == "Name")
                {
                    column.Visibility = Visibility.Visible;
                    column.Width = 300;
                }
                else
                {
                    column.Visibility = Visibility.Hidden;
                }
            }
        }
    }
}
