﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Grid_Module.ViewModel;
using UserControl = System.Windows.Controls.UserControl;

namespace Grid_Module
{
    /// <summary>
    /// Interaction logic for OntologyGrid.xaml
    /// </summary>
    public partial class OntologyGrid : UserControl
    {
        private OntologyGridViewModel viewModel;

        private clsDataWork_Grid dataWorkGrid;

        public clsDataWork_Grid DataWorkGrid
        {
            get { return dataWorkGrid; }
            set
            {
                dataWorkGrid = value;
                viewModel.DataWorkGrid = dataWorkGrid;
            }
        }
        public OntologyGrid()
        {
            InitializeComponent();
            viewModel = new OntologyGridViewModel();
            DataContext = viewModel;

        }

        private void DataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            viewModel.ConfigureColumn(e.Column);
        }
    }
}
