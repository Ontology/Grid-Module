﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System.Windows.Forms;
using Grid_Module.Grid;
using OntoMsg_Module;

namespace Grid_Module.ViewModel
{
    public class OntologyGridViewModel : BaseBinding
    {
        private clsDataWork_Grid dataWorkGrid;

        private clsFields fields = new clsFields();
        private clsDataTypes dataTypes = new clsDataTypes();

        private ObservableCollection<object> itemList;
        public ObservableCollection<object> ItemList
        {
            get { return itemList; }
            set
            {
                itemList = value;
                OnPropertyChanged("ItemList");
            }
        } 

        public clsDataWork_Grid DataWorkGrid
        {
            get
            {
                return dataWorkGrid;
            }
            set
            {
                dataWorkGrid = value;
                OnPropertyChanged("DataWorkGrid");
            }
        }
        public OntologyGridViewModel()
        {
            //MessageManager.ObjectPublishers.Where(publ => publ.SubscriberType == SubscriberType.GridView && (publ.ConcreteSubscriber == null || publ.ConcreteSubscriber == this)).ToList().ForEach(
            //    publ =>
            //    {
            //        publ._showObjectsOfClass += publ__showObjectsOfClass;
            //    });
        }

        void publ__showObjectsOfClass(clsOntologyItem classItem)
        {
            var result = dataWorkGrid.GetData_ObjectsOfClass(classItem);
            ItemList = new ObservableCollection<object>( dataWorkGrid.ObjectsOfClass);
        }

        public void ConfigureColumn(DataGridColumn column)
        {
            GridConfigurator.ConfigureVisibility(column, typeof (clsOntologyItem));
        }
        
    }
}
