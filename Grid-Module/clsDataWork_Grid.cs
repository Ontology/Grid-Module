﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System.Runtime.InteropServices;

namespace Grid_Module
{
    public class clsDataWork_Grid
    {
        private clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_Objects;
        private OntologyModDBConnector objDBLevel_AttributesOfClasses;
        private OntologyModDBConnector objDBLevel_ObjAtts;
        private OntologyModDBConnector objDBLevel_ObjRels;

        public List<clsOntologyItem> ObjectsOfClass { get { return objDBLevel_Objects.Objects1; } }
        public List<clsObjectAtt> ObjAttsOfClass { get { return objDBLevel_ObjAtts.ObjAtts; } }
        public List<clsObjectRel> ObjRelsOfClass { get { return objDBLevel_ObjRels.ObjectRels; } } 

        public clsOntologyItem GetData_ObjectsOfClass(clsOntologyItem OITem_Class)
        {

            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            ObjectsOfClass.Clear();

            objOItem_Result = objDBLevel_Objects.GetDataObjects(new List<clsOntologyItem> { new clsOntologyItem { GUID_Parent = OITem_Class.GUID, Type = objLocalConfig.Globals.Type_Object } });

            return objOItem_Result;
        }

        public clsOntologyItem GetData_AttributesOfObjects(clsOntologyItem OItem_Class)
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();


            objOItem_Result = objDBLevel_ObjAtts.GetDataObjectAtt(new List<clsObjectAtt> { new clsObjectAtt { ID_Class = OItem_Class.GUID } }, doIds: false);

            return objOItem_Result;
        }

        public clsOntologyItem GetData_ObjectRelsOfObjects(clsOntologyItem OItem_Class)
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();


            objOItem_Result = objDBLevel_ObjRels.GetDataObjectRel(new List<clsObjectRel> { new clsObjectRel { ID_Parent_Object = OItem_Class.GUID } }, doIds: false);

            return objOItem_Result;
        }

        public List<clsOntologyItem> GetDataAttributeTypesOfClass(clsOntologyItem oItemClass)
        {
            var searchAttributeTypes = new List<clsClassAtt>
            {
                new clsClassAtt
                {
                    ID_Class = oItemClass.GUID
                }
            };

            var result = objDBLevel_ObjAtts.GetDataClassAtts();

            return objDBLevel_AttributesOfClasses.AttributeTypes;
        }

        public clsDataWork_Grid(Globals globals)
        {
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            Initialize();
        }

        public clsDataWork_Grid(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        private void Initialize()
        {
            objDBLevel_Objects = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_AttributesOfClasses = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_ObjAtts = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_ObjRels = new OntologyModDBConnector(objLocalConfig.Globals);
        }
    }
}
